﻿using PersonalMoneyManagerMVC.Data.Interface;
using PersonalMoneyManagerMVC.Data.Repository;
using PersonalMoneyManagerMVC.Data.Service;
using PersonalMoneyManagerMVC.Entity.Model;
using System.Web.Mvc;
using PersonalMoneyManagerMVC.Data.Data.Context;
using Unity;
using Unity.AspNet.Mvc;
using Unity.Lifetime;

namespace PersonalMoneyManagerMVC.Web
{
    public class UnityConfiguration
    {
        public static void RegisterComponents()
        {

            var container = new UnityContainer();
            container.RegisterType<IRepository<Wallet>, SqlRepository<Wallet>>();
            container.RegisterType<IRepository<Operation>, SqlRepository<Operation>>();
            container.RegisterType<IRepository<Category>, SqlRepository<Category>>();
            container.RegisterType<IUpdateWalletBalanceService, UpdateWalletBalanceService>();
            container.RegisterType<IRepository<Log>, SqlRepository<Log>>();
            container.RegisterType<ILogger, DatabaseLoggerService>();
            
            //container.Resolve<ContextSingleton>();
            container.RegisterType<PersonalMoneyManagerDbContext>(new ContainerControlledLifetimeManager());
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}