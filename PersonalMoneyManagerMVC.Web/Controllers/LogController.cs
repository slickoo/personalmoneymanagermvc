﻿using PersonalMoneyManagerMVC.Data.Interface;
using PersonalMoneyManagerMVC.Entity.Model;
using PersonalMoneyManagerMVC.Entity.ViewModel.Logs;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PersonalMoneyManagerMVC.Web.Controllers
{
    public class LogController : Controller
    {
        private IRepository<Log> _logRepository;

        public LogController(IRepository<Log> logRepository)
        {
            _logRepository = logRepository;
        }
        public async Task<ActionResult> ShowLog(int page)
        {
            var itemsOnPage = 10;

            var logListViewModel = new LogListViewModel
            {
                logs = _logRepository.Get()
                    .OrderByDescending(x => x.Created)
                    .Skip((page - 1) * itemsOnPage)
                    .Select(x => new LogViewModel
                        {
                            Message = x.Message,
                            Type = x.Type.ToString(),
                            OldName = x.OldName,
                            NewName = x.NewName,
                            Created = x.Created.ToString()
                        }
                    ).Take(itemsOnPage)
                    .ToList(),

                PageInfo = new PageInfo
                {
                    Page = page,
                    PageCount = (int)Math.Ceiling((double)_logRepository.Get().Count() / itemsOnPage)
                }
            };


            return View(logListViewModel);

        }
    }
}