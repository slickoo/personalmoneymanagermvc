﻿using PersonalMoneyManagerMVC.Data.Interface;
using PersonalMoneyManagerMVC.Entity.Model;
using PersonalMoneyManagerMVC.Entity.ViewModel.Dashboard;

using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using PersonalMoneyManagerMVC.Entity.ViewModel.Logs;


namespace PersonalMoneyManagerMVC.Web.Controllers
{
    public class HomeController : Controller
    {
        private IRepository<Wallet> _walletRepository;
        private IRepository<Operation> _operationRepository;
        private IRepository<Log> _logRepository;

        public HomeController(IRepository<Wallet> walletRepository,
                              IRepository<Operation> operationRepository,
                              IRepository<Log> logRepo)
        {
            _walletRepository = walletRepository;
            _operationRepository = operationRepository;
            _logRepository = logRepo;
        }
        [HttpGet]

        public async  Task<ActionResult> Dashboard()
        {
            var dashVM = new DashViewModel();            

            var operations = await _operationRepository
                .Get()
                .Include("Category")
                .OrderByDescending(x => x.Created)
                .Take(10)
                .ToListAsync();

            var wallets = _walletRepository
                .Get()
                .OrderByDescending(x => x.Created)
                .Take(5)
                .ToList();

            dashVM.Operations = operations
                .Select(item => new DashOperationVM
                {
                    Name = item.Name,
                    CategoryName = item.Category.Name,
                    CategoryType = item.Category.WithdrawOrProfit.ToString(),
                    Created = $"{item.Created.ToShortDateString()} {item.Created.ToShortTimeString()}",
                    Id = item.Id,
                    Summ = item.Summ,
                    Comment = item.Comment
                })
            .ToList();            

            foreach (var item in wallets)
            {
                var wallet = new DashWalletVM
                {
                    Id = item.Id,
                    Name = item.Name,
                    Balance = item.Balance,
                    Created = $"{item.Created.ToShortDateString()} {item.Created.ToShortTimeString()}",
                    Currency = item.Currency.ToString(),
                    Type = item.Type.ToString()

                };
                dashVM.Wallets.Add(wallet);
            }
           
            return View(dashVM);
        }

        

       


    }
}
