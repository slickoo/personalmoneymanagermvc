﻿using PersonalMoneyManagerMVC.Data.Interface;
using PersonalMoneyManagerMVC.Entity.DTO.Operation;
using PersonalMoneyManagerMVC.Entity.Model;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using PersonalMoneyManagerMVC.Entity.Enum;

namespace PersonalMoneyManagerMVC.Web.Controllers
{
    public class OperationController : Controller
    {
        private readonly IRepository<Wallet> _walletRepository;
        private readonly IRepository<Operation> _operationRepository;
        private readonly IRepository<Category> _categoryRepository;
        private readonly IUpdateWalletBalanceService _updateBalanceService;
        private readonly ILogger _logger;
        

        public OperationController(IRepository<Wallet> walletRepo,
                                   IRepository<Operation> operationRepo,
                                   IRepository<Category> categoryRepo,
                                   IUpdateWalletBalanceService service,
                                   ILogger logger)
        {
            _operationRepository = operationRepo;
            _categoryRepository = categoryRepo;
            _walletRepository =  walletRepo;
            _updateBalanceService = service;
            _logger = logger;
        }



        [HttpGet]
        public async Task<ActionResult> Create(Guid id)
        {
            var wallet = _walletRepository.Get().SingleOrDefault(x => x.Id.Equals(id));
            
                if (wallet == null)
                {
                    await _logger.Log(LogType.Error, "Wallet does not exist", null, id.ToString(), "");
                    throw new Exception("Wallet does not exist");
                }


            var listOfCategories = await _categoryRepository.GetList();

            var createOperationDto = new CreateOperationDTO
            {
                Categories = listOfCategories
                    .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name })
                    .ToList()
            };
            return View(createOperationDto);
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateOperationDTO operationDto, Guid id)
        {
            var operationCategoryGuidId = Guid.Parse(operationDto.CategoryId);
         

            var operation = new Operation
            {
                Name = operationDto.Name,
                Comment = operationDto.Comment,
                Summ = operationDto.Summ,
                WalletId = id,
                PayPlace = operationDto.PayPlace,
                CategoryId = operationCategoryGuidId
            };


             _operationRepository.Add(operation);

            await _operationRepository.Save();

            await _logger.Log(LogType.Info, "Operation add", operation,operation.Name,"");
            await _updateBalanceService.UpdateBalance();
            

            return RedirectToAction("Details", "Wallet", new { id,page=1 });
        }

        

    }
}