﻿using PersonalMoneyManagerMVC.Data.Interface;
using PersonalMoneyManagerMVC.Entity.DTO.Category;
using PersonalMoneyManagerMVC.Entity.Enum;
using PersonalMoneyManagerMVC.Entity.Model;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PersonalMoneyManagerMVC.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IRepository<Category> _categoryRepository;
        private ILogger _logger;
        

        public CategoryController(IRepository<Category> categoryRepo,ILogger logger)
        {
            _categoryRepository = categoryRepo;
            _logger = logger;
        }
        // GET: Category
      
        [HttpGet]
        public async Task<ActionResult> Create()
        {

            var createCategoryDto = new CreateCategoryDTO();
            return View(createCategoryDto);
        }

        [HttpPost]

        public async Task<ActionResult> Create(CreateCategoryDTO categoryDTO)
        {
            Category categoryEntity = new Category
            {
                Name = categoryDTO.Name,
                Icon = categoryDTO.SelectedIcon,
                IconColor = categoryDTO.SelectedColor,
                WithdrawOrProfit = categoryDTO.WithdrawOrProfit

            };

         
            _categoryRepository.Add(categoryEntity);
            
            await _categoryRepository.Save();
            await _logger.Log(LogType.Info, "Category add", categoryEntity,categoryEntity.Name,"");
            // todo: redirect to category page, not wallets
            return RedirectToAction("ShowAllWallets", "Wallet");
        }
    }
}