﻿using PersonalMoneyManagerMVC.Data.Interface;
using PersonalMoneyManagerMVC.Entity.DTO.Account;
using PersonalMoneyManagerMVC.Entity.Enum;
using PersonalMoneyManagerMVC.Entity.Helper;
using PersonalMoneyManagerMVC.Entity.Model;
using PersonalMoneyManagerMVC.Entity.ViewModel.Operation;
using PersonalMoneyManagerMVC.Entity.ViewModel.Wallet;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace PersonalMoneyManagerMVC.Web.Controllers
{
    public class WalletController : Controller
    {
        private readonly IRepository<Wallet> _walletRepository;
        private readonly ILogger _logger;
        private readonly IUpdateWalletBalanceService _updateBalanceService;
      
        public  WalletController(IRepository<Wallet> walletRepo,IUpdateWalletBalanceService service, ILogger logger)
        {
            this._walletRepository = walletRepo;
            _updateBalanceService = service;
            _logger = logger;

        }

        
        //Create action

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateWalletDTO walletDto)
        {
            Wallet wallet = new Wallet
            {
                Name = walletDto.Name,
                Currency = walletDto.Currency,
                Type = walletDto.Type
            };


         

            _walletRepository.Add(wallet);
            await _walletRepository.Save();
            await _logger.Log(LogType.Info, "Created account", wallet,wallet.Name,"");
           
            return RedirectToAction("Details",new {id=wallet.Id,page=1});
        }


        //Display all wallets 
        [HttpGet]
        public async  Task<ActionResult> ShowAllWallets()
        {
            await _updateBalanceService.UpdateBalance();
            var wallets = await _walletRepository.Get().Select(x =>  new WalletDetailsViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Balance = x.Balance,
                Created = x.Created.ToString(),
                Currency = x.Currency.ToString(),
                Type = x.Type.ToString()
                

            }).ToListAsync();

            wallets.ForEach(x=>x.SelectIcon());
            return View(wallets);
        }



        //Edit wallet 
        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            
                var wallet = _walletRepository
                    .Get()
                    .SingleOrDefault(x => x.Id == id);

            try
            {

          
            if (wallet == null)
            {
                
                await _logger.Log(LogType.Error, "Wallet not exists", null, id.ToString(), "");
                throw new ArgumentException("wtf?");
                }
            }
            catch (Exception e)

            {
                return RedirectToAction("ShowAllWallets","Wallet");

            }
            var walletDto = new CreateWalletDTO
            {
                Currency = wallet.Currency,
                Name = wallet.Name,
                Type = wallet.Type
            };


            return View(walletDto);
        }



        [HttpPost]
        public async Task<ActionResult> Edit(CreateWalletDTO walletDto, Guid id)
        {
            var walletCheck = _walletRepository.Get()
                                        .SingleOrDefault(x => x.Id.Equals(id));

            if (walletCheck == null)
            {
               await _logger.Log(LogType.Error, "Wallet not exists", null, id.ToString(), "");
               
            }
            Wallet wallet = new Wallet
            {
                Id = id,
                Name = walletDto.Name,
                Currency = walletDto.Currency,
                Type = walletDto.Type
            };

            
            await _walletRepository.Update(wallet);
            await _walletRepository.Save();
            await _logger.Log(LogType.Info, "Edited account", wallet,walletCheck.Name,wallet.Name);

           
            return RedirectToAction("Details","Wallet",new {id=wallet.Id,page=1});
        }


        //Wallet details
        [HttpGet]
        public async Task<ActionResult> Details(Guid id,int page)
        {
          
            var wallet =   _walletRepository
                .Get()
                .Include(x => x.OperationsHistory)
                .Include(x => x.OperationsHistory.Select(y => y.Category))
                .SingleOrDefault(x => x.Id == id);

            if (wallet == null)
            {
                
                await _logger.Log(LogType.Error, "Wallet does not exists", null, id.ToString(), "");
                throw new Exception("Wallet does not exist");
            }

            var walletViewModel = new WalletDetailsViewModel
            {
                Id = wallet.Id,
                Name = wallet.Name,
                Created = wallet.Created.ToShortDateString(),
                Currency = wallet.Currency.ToString(),
                Type = wallet.Type.ToString(),
                Balance = wallet.Balance,
                PageInfo = new PageInfo
                {
                    Page =page,
                    PageCount =(int)Math.Ceiling((double)wallet.OperationsHistory.Count/5)
                },
                OperationHistory = wallet.OperationsHistory
                    .OrderByDescending(x=>x.Created)
                    .Skip((page-1)*5)
                    .Select(x => new OperationDetailsViewModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Comment = x.Comment,
                        PayPlace = x.PayPlace,
                        Summ = x.Summ,
                        Created = x.Created.ToShortDateString(),
                        Icon = x.Category.Icon,
                        IconColor = x.Category.IconColor
                    })
                    .Take(5)
                    .ToList()
                
            };
            
            walletViewModel.SelectIcon();

            return View(walletViewModel);
        }

        
}

}