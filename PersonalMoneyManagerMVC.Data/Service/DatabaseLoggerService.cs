﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PersonalMoneyManagerMVC.Data.Interface;
using PersonalMoneyManagerMVC.Entity.Enum;
using PersonalMoneyManagerMVC.Entity.Model;

namespace PersonalMoneyManagerMVC.Data.Service
{
    public class DatabaseLoggerService:ILogger
    {
        private IRepository<Log> _logRepository;
        private JsonSerializerSettings settings;
        public DatabaseLoggerService(IRepository<Log> repo)
        {
            _logRepository = repo;
            settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                Formatting = Formatting.Indented
            };
        }
        public async Task Log(LogType type, string msg,Object obj,string oldName,string newName)
        {
            _logRepository.Add(new Log
            {
                    Message = msg,
                    Type = type,
                    SerializedObject = JsonConvert.SerializeObject(obj,settings),
                    NewName = newName,
                    OldName = oldName
            });

            await _logRepository.Save();
        }
    }
}
