﻿using PersonalMoneyManagerMVC.Data.Interface;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using PersonalMoneyManagerMVC.Entity.Enum;
using PersonalMoneyManagerMVC.Entity.Model;

namespace PersonalMoneyManagerMVC.Data.Service
{
    public class UpdateWalletBalanceService : IUpdateWalletBalanceService
    {
        private readonly IRepository<Wallet> _walletRepository;

        public UpdateWalletBalanceService(IRepository<Wallet> walletRepo)
        {
            _walletRepository = walletRepo;
        }

        public async Task UpdateBalance()
        {
            var wallets = _walletRepository
                .Get()
                .Include(x => x.OperationsHistory)
                .Include(x => x.OperationsHistory.Select(y => y.Category));

            foreach (var wallet in wallets)
            {
                float newBalance = 0;

                foreach (var operation in wallet.OperationsHistory)
                {
                    if (operation.Category.WithdrawOrProfit.Equals(CategoryType.Withdraw))
                    {
                        newBalance -= operation.Summ;
                    }
                    else
                    {
                        newBalance += operation.Summ;
                    }
                }

                wallet.Balance = newBalance;
            }

            await _walletRepository.Save();
        }
    }
}


