﻿using PersonalMoneyManagerMVC.Data.Interface;


using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using PersonalMoneyManagerMVC.Data.Data.Context;
using PersonalMoneyManagerMVC.Entity.Model;

namespace PersonalMoneyManagerMVC.Data.Repository
{
    public class SqlRepository<TEntity> : IRepository<TEntity> where TEntity: class 
    {
        private PersonalMoneyManagerDbContext _context;

        private DbSet<TEntity> _dbSet;
        public SqlRepository(PersonalMoneyManagerDbContext context)
        {
            _context = context;
            _dbSet=context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public void Delete(Guid entityId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method returns query for all wallets 
        /// (!) Not enumerable
        /// Query means that data will be fetched from db onl on first enumeration.
        /// 
        /// User can apply any LINQ method at thie query
        /// like .Where() or .Include() 
        /// </summary>
        /// <returns>Query for wallets</returns>
        public IQueryable<TEntity> Get()
        {
            return _dbSet.AsQueryable();
        }

        public async Task<IEnumerable<TEntity>> GetList()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        //// search for EntryState.Modifyed approach (1 line update)
        public async Task Update(TEntity entity)
        {
            //var selectedWallet = _dbSet.Find(entity.Id);
            //selectedWallet.Name = entity.Name;
            //selectedWallet.Currency = entity.Currency;
            //selectedWallet.Type = entity.Type;
        }

        public  bool EntityExists(TEntity entity)
        {
            if (_dbSet.Local.Any(e => e.Equals(entity))) return true;

            return false;
        }
    }
}

