﻿using PersonalMoneyManagerMVC.Data.Data.DatabaseInitializer;

namespace PersonalMoneyManagerMVC.Data.Data.Context
{
    using Entity.Model;
    using System.Data.Entity;

    public class PersonalMoneyManagerDbContext : DbContext
    {
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Operation> Operations { get; set; }
        public DbSet<Log> Logs { get; set; }

        public PersonalMoneyManagerDbContext()
            : base("name=PersonalMoneyManagerDbContext")
        {
            Database.SetInitializer<PersonalMoneyManagerDbContext>(new DatabaseInitializer.DatabaseInitializer());
        }

        // add seed method for initial database seed
        // (default wallets, ..)
 
    }

  
}