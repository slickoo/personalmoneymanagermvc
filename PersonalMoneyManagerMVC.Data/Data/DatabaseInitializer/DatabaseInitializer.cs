﻿
using PersonalMoneyManagerMVC.Entity.Enum;
using PersonalMoneyManagerMVC.Entity.Model;
using System.Data.Entity;
using PersonalMoneyManagerMVC.Data.Data.Context;

namespace PersonalMoneyManagerMVC.Data.Data.DatabaseInitializer
{
    public class DatabaseInitializer: CreateDatabaseIfNotExists<PersonalMoneyManagerDbContext>
    {
        
        protected override void Seed(PersonalMoneyManagerDbContext context)
        {
            context.Categories.Add(new Category
                {
                    Icon = "glyphicon glyphicon-home",
                    IconColor = "black",
                    Name = "Communal expenses",
                    WithdrawOrProfit = CategoryType.Withdraw
                }

            );
            context.Categories.Add(new Category
                {
                    Icon = "glyphicon glyphicon-heart",
                    IconColor = "red",
                    Name = "Treatment",
                    WithdrawOrProfit = CategoryType.Withdraw
                }

            );

            context.Categories.Add(new Category
                {
                    Icon = "glyphicon glyphicon-ruble",
                    IconColor = "yellow",
                    Name = "Salary",
                    WithdrawOrProfit = CategoryType.Profit
                }

            );
        }
    }
}
