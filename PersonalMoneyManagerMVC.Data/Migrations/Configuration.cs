using PersonalMoneyManagerMVC.Entity.Enum;
using PersonalMoneyManagerMVC.Entity.Model;
using  PersonalMoneyManagerMVC.Data.Data.Context;

namespace PersonalMoneyManagerMVC.Data.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<PersonalMoneyManagerDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(PersonalMoneyManagerDbContext context)
        {
           
        }
    }
}
