﻿

using System.Threading.Tasks;

namespace PersonalMoneyManagerMVC.Data.Interface
{
    public interface IUpdateWalletBalanceService
    {
        Task UpdateBalance();
    }

}
