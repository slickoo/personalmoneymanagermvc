﻿using PersonalMoneyManagerMVC.Entity.Enum;
using System;
using System.Threading.Tasks;

namespace PersonalMoneyManagerMVC.Data.Interface
{
    public interface ILogger
    {
        Task Log(LogType type, string msg,Object obj,string oldName, string newName);
    }
}
