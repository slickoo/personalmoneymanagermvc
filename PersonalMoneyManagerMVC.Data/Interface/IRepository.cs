﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalMoneyManagerMVC.Data.Interface
{
    public interface IRepository<TEntity> where TEntity:class 
    {

        Task<IEnumerable<TEntity>> GetList();

        void Add(TEntity entity);

        void Delete(Guid entityId);

        Task Update(TEntity entity);

        Task Save();

        bool EntityExists(TEntity entity);

        IQueryable<TEntity> Get();
    }
}
