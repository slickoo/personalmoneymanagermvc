﻿namespace PersonalMoneyManagerMVC.Entity.Model
{
    public class PageInfo
    {
        public int Page { get; set; }
   
        public int PageCount { get; set; }

    }
}
