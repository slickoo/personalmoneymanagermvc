﻿using System;

namespace PersonalMoneyManagerMVC.Entity.Model
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime Created { get; private set; } = DateTime.Now;
        public int Version { get; set; }
    }
}
