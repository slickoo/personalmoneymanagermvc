﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalMoneyManagerMVC.Entity.Model
{
    public interface IEntityWithIcon
    {
        string Icon { get; set; }
        string IconColor { get; set; }
    }
}
