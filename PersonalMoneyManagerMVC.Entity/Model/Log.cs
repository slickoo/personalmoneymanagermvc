﻿using System;
using PersonalMoneyManagerMVC.Entity.Enum;

namespace PersonalMoneyManagerMVC.Entity.Model
{
    public  class Log:BaseEntity
    {
        public string Message { get; set; }
        public  LogType Type { get; set; }
        public string SerializedObject { get; set; }
        public string OldName { get; set; }
        public string NewName { get; set; } 
    }
}
