﻿using System;

namespace PersonalMoneyManagerMVC.Entity.Model
{
    public class Operation : BaseEntity
    {
        public string Name { get; set; }
        public float Summ { get; set; }
        public string Comment { get; set; }
        public string PayPlace { get; set; }

        // need link to Id
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }

        //Wallet Wallet
        public Wallet OperationWallet { get; set; }
        public Guid WalletId { get; set; }
       
    }
}
