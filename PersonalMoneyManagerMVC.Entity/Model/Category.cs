﻿using PersonalMoneyManagerMVC.Entity.Enum;

namespace PersonalMoneyManagerMVC.Entity.Model
{
    public class Category : BaseEntity, IEntityWithIcon
    {
        public string Name { get; set; }
        public CategoryType WithdrawOrProfit { get; set; }

        public string Icon { get; set; }
        public string IconColor { get; set; }
    }
}
