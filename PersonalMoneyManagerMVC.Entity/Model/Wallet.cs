﻿using PersonalMoneyManagerMVC.Entity.Enum;
using System.Collections.Generic;

namespace PersonalMoneyManagerMVC.Entity.Model
{
    public class Wallet : BaseEntity
    {
        public string Name { get; set; }
        public float Balance { get; set; }

        public WalletCurrency Currency { get; set; }
        public WalletType Type { get; set; }

        public bool IsIncludedInTotalBalance { get; set; }

        public ICollection<Operation> OperationsHistory { get; set; }


    }
}
