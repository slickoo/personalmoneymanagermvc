﻿using PersonalMoneyManagerMVC.Entity.ViewModel.Wallet;

namespace PersonalMoneyManagerMVC.Entity.Helper
{
    public  static class SelectIconHelper
    {
        public static void SelectIcon(this WalletDetailsViewModel wallet)
        {
            switch (wallet.Type)
            {
                case "Cash":
                    wallet.Icon = "glyphicon glyphicon-folder-open";
                    wallet.IconColor = "black";
                    break;

                case "Card":
                    wallet.Icon = "glyphicon glyphicon-credit-card";
                    wallet.IconColor = " blue";
                    break;

                case "Deposit":
                    wallet.Icon = "glyphicon glyphicon-piggy-bank";
                    wallet.IconColor = "green";
                    break;


            };
        }
    }
}
