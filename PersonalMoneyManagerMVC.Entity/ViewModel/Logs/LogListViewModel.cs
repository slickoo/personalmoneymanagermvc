﻿using PersonalMoneyManagerMVC.Entity.Model;
using System.Collections.Generic;


namespace PersonalMoneyManagerMVC.Entity.ViewModel.Logs
{
    public class LogListViewModel
    {
        public List<LogViewModel> logs { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}
