﻿using System;

namespace PersonalMoneyManagerMVC.Entity.ViewModel.Logs
{
    public class LogViewModel
    {
        public string Message { get; set; }
        public string Type { get; set; }
        public string OldName { get; set; }
        public string NewName { get; set; }
        public string Created { get; set; }
    }
}
