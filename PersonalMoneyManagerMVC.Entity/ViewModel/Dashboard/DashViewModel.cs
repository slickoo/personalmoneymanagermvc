﻿using System.Collections.Generic;

namespace PersonalMoneyManagerMVC.Entity.ViewModel.Dashboard
{
    public class DashViewModel
    {
        public ICollection<DashWalletVM> Wallets { get; set; }
        public ICollection<DashOperationVM> Operations { get; set; }

        public DashViewModel()
        {
            Wallets= new List<DashWalletVM>();
            Operations = new List<DashOperationVM>();
        }
    }
   
}
