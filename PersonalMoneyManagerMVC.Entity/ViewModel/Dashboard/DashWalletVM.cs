﻿using System;

namespace PersonalMoneyManagerMVC.Entity.ViewModel.Dashboard
{
    public class DashWalletVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public float Balance { get; set; }
        public string Created { get; set; }
        public string Currency { get; set; }
        public string Type { get; set; }
    }
}
