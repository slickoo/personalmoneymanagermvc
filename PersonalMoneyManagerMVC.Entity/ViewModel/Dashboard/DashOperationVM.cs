﻿using System;

namespace PersonalMoneyManagerMVC.Entity.ViewModel.Dashboard
{
    public class DashOperationVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public float Summ { get; set; }
        public string CategoryName { get; set; }
        public string CategoryType { get; set; }
        public string Created { get; set; }
        public string Comment { get; set; }
    }
}
