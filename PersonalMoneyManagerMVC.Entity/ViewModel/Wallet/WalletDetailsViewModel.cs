﻿using PersonalMoneyManagerMVC.Entity.Model;
using PersonalMoneyManagerMVC.Entity.ViewModel.Operation;
using System;
using System.Collections.Generic;

namespace PersonalMoneyManagerMVC.Entity.ViewModel.Wallet
{
    public class WalletDetailsViewModel

    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Currency { get; set; }
        public string Type { get; set; }
        public string Created { get; set; }
        public float Balance { get; set; }

       

        public IEnumerable<OperationDetailsViewModel> OperationHistory { get; set; }
        public string Icon { get; set ; }
        public string IconColor { get; set ; }

        public PageInfo PageInfo { get; set; }
    }
}
