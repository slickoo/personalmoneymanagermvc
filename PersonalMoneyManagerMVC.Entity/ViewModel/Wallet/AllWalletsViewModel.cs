﻿using PersonalMoneyManagerMVC.Entity.Model;
using System.Collections.Generic;

namespace PersonalMoneyManagerMVC.Entity.ViewModel.Wallet
{
    public class AllWalletsViewModel
    {
        public IEnumerable<WalletDetailsViewModel> WalletDetailsViewModels { get; set; }
        public PageInfo PageInfo { get; set; }

        public AllWalletsViewModel()
        {
            
        }
    }
}
