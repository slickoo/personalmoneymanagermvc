﻿using System;
using PersonalMoneyManagerMVC.Entity.Model;

namespace PersonalMoneyManagerMVC.Entity.ViewModel.Operation
{
    public class OperationDetailsViewModel : IEntityWithIcon
    {
        
        public Guid Id { get; set; }
        public string Name { get; set; }
        public float Summ { get; set; }
        public string Comment { get; set; }
        public string PayPlace { get; set; }
        public string Created { get; set; }

        public string Icon { get; set; }
        public string IconColor { get; set; }
    }
}
