﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace PersonalMoneyManagerMVC.Entity.DTO.Operation
{
    public class CreateOperationDTO
    {
        public string Name { get; set; }

        [RegularExpression(@"^\d+\.\d{0,2}$")]
        public float Summ { get; set; }
        public string Comment { get; set; }
        public string PayPlace { get; set; }

        public string CategoryId { get; set; }
        public List<SelectListItem> Categories { get; set; }

    }

   
}
