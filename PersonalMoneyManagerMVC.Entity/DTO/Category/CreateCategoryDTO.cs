﻿using PersonalMoneyManagerMVC.Entity.Enum;
using PersonalMoneyManagerMVC.Entity.Model;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PersonalMoneyManagerMVC.Entity.DTO.Category
{
    public class CreateCategoryDTO
    {
        public string Name { get; set; }
        public CategoryType WithdrawOrProfit { get; set; }
        public string SelectedIcon { get; set; }
        public string SelectedColor { get; set; }
        public List<SelectListItem> IconList { get; set; }
        public List<SelectListItem> ColorList { get; set; }

        public CreateCategoryDTO()
        {
            FillIconCombo();
            FillColorCombo();
            
        }

        public void FillIconCombo()
        {
            var ListOfIcons = new List<string>();
            ListOfIcons.Add("glyphicon glyphicon-home");
            ListOfIcons.Add("glyphicon glyphicon-heart");
            ListOfIcons.Add("glyphicon glyphicon-road");
            ListOfIcons.Add("glyphicon glyphicon-shopping-cart");
            //ListOfIcons.Add(new Icon
            //    {
            //        Name = "Health",
            //        Value = "glyphicon glyphicon-heart"
            //    }
            //);
            //ListOfIcons.Add(new Icon
            //    {
            //        Name = "Travel",
            //        Value = "glyphicon glyphicon-road"
            //    }
            //);
            //ListOfIcons.Add(new Icon
            //    {
            //        Name = "Shopping",
            //        Value = "glyphicon glyphicon-shopping-cart"
            //    }
            //);

            IconList = ListOfIcons
                .Select(x => new SelectListItem { Value = x, Text = x })
                .ToList();
        }

        public void FillColorCombo()
        {
            var ListOfColors = new List<string>();
            ListOfColors.Add("Red");
            ListOfColors.Add("Green");
            ListOfColors.Add("Blue");
            ListOfColors.Add("Yellow");

            ColorList = ListOfColors
                .Select(x => new SelectListItem {Value = x, Text = x})
                .ToList();
        }

       
    }
}
