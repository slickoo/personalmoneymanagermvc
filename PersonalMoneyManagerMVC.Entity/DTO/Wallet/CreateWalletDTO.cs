﻿using System.ComponentModel.DataAnnotations;
using PersonalMoneyManagerMVC.Entity.Enum;

namespace PersonalMoneyManagerMVC.Entity.DTO.Account
{
    public class CreateWalletDTO
    {
        [Required]
        public string Name { get; set; }
        public WalletCurrency Currency { get; set; }
        public WalletType Type { get; set; }

    }
}