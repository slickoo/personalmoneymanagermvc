﻿namespace PersonalMoneyManagerMVC.Entity.Enum
{
    public enum WalletType
    {
        Cash = 1,
        Card = 2,
        Deposit = 3,
        Default = 0
    }
}