﻿namespace PersonalMoneyManagerMVC.Entity.Enum
{
    public enum WalletCurrency
    {
        MDL = 1,
        USD = 2,
        EUR = 3
    }
}
